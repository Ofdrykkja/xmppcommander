#!/usr/bin/python
# -*- coding: utf-8 -*-

import json
import logging
import subprocess as sp
from pyxmpp2.settings import XMPPSettings
from pyxmpp2.client import Client
from pyxmpp2.ext.version import VersionProvider
from pyxmpp2.jid import JID
from pyxmpp2.message import Message
#check useless import
from pyxmpp2.interfaces import XMPPFeatureHandler
from pyxmpp2.streamevents import AuthorizedEvent, DisconnectedEvent
from pyxmpp2.interfaces import presence_stanza_handler, message_stanza_handler
from pyxmpp2.interfaces import EventHandler, event_handler, QUIT

class XMPPCommander(EventHandler, XMPPFeatureHandler):

    def __init__(self, config):
        self.config = json.load(open(config))
        self.init_bot()

    def __repr__(self):
        return "[JSON] %s" % self.config

    def init_bot(self):
        settings = XMPPSettings({
                            "software_name": "XMPPComander",
                            "password": self.config["password"],
                            "starttls": True,
                            #"tls_require": True,
                            "tls_verify_peer": False
                            })
        version_provider = VersionProvider(settings)
        self.client = Client(JID(self.config["jid"]), [self, version_provider], settings)

    def run(self):
        self.client.connect()
        self.client.run()

    def disconnect(self):
        self.client.disconnect()
        self.client.run(timeout = 2)

    @message_stanza_handler()
    def handle_message(self, stanza):
        if stanza.subject:
            subject = u"Re: " + stanza.subject
        else:
            subject = None

        print "FROM: %s BODY: %s" % (stanza.from_jid, stanza.body)

        try:
            script = self.config[stanza.from_jid.local + "@" + stanza.from_jid.domain][stanza.body]
            process = sp.Popen(script, shell = True, stdout = sp.PIPE, stderr = sp.PIPE)
            msg_body = process.communicate()[0]
            #msg_body = os.system("./" + script)
        except KeyError:
            print "KeyError"
            return

        msg = Message(stanza_type = stanza.stanza_type,
                        from_jid = stanza.to_jid, to_jid = stanza.from_jid,
                        subject = subject, body = msg_body,
                        thread = stanza.thread)
        return msg

    @event_handler(DisconnectedEvent)
    def handle_disconnected(self, event):
        return QUIT

    @event_handler()
    def handle_all(self, event):
        logging.info(u"-- {0}".format(event))



def main():
    logging.basicConfig(level = logging.INFO)
    commander = XMPPCommander("conf.json")
    try:
        commander.run()
    except KeyboardInterrupt:
        commander.disconnect()


if __name__ == "__main__":
    main()
