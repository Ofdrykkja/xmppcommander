1. Install pyxmpp2:
    git clone https://github.com/Jajcus/pyxmpp2.git
    python setup.py build
    sudo python setup.py install
2. Install python-dnspython (optional):
    sudo aptitude install python-dnspython
3. Write conf.json (see example file)
4. run ./XMPPComander.py
